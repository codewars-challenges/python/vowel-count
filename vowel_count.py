def getCount(inputStr):
    return len([c for c in list(inputStr) if c in ['a', 'e', 'i', 'o', 'u']])
